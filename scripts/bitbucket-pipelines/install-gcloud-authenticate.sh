#!/bin/bash -e

# Install gcloud and authenticate using the variable of which value is the base64 encoded content of the service account key file

# Usage:
#   ./install-gcloud-authenticate KEY_FILE_VARIABLE_NAME

KEY_FILE_VARIABLE_NAME=$1
if [ -z "$KEY_FILE_VARIABLE_NAME" ]; then
    echo "Missing varible name for the base64 encoded service account content"
    exit 1
fi

echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
apt-get install apt-transport-https ca-certificates gnupg
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
apt-get update && apt-get install -y google-cloud-sdk
gcloud -v

GCLOUD_CONFIG_FOLDER=$HOME/.config/gcloud
mkdir -p $GCLOUD_CONFIG_FOLDER

CREDENTIAL_FILE="$GCLOUD_CONFIG_FOLDER/application_default_credentials.json"
echo ${!KEY_FILE_VARIABLE_NAME} | base64 --decode --ignore-garbage > $CREDENTIAL_FILE

gcloud auth activate-service-account --key-file=$CREDENTIAL_FILE
