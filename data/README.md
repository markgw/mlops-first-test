# Data

This folder contains data related files such as:
- Actual data files to be used locally (**should be ignored by Git**)
- [`.dvc` files](https://dvc.org/doc/user-guide/project-structure/dvc-files) (**should be tracked by Git**) to track data files and directories if using [DVC](https://dvc.org/).
