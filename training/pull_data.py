# noqa: E999
import json
import subprocess
import typing
from pathlib import Path

import dvc.api

from .config import get_config
import click

from .logger import logger


def dvc_list_tracked_files(
    repo: typing.Union[Path, str],
    path: str,
    rev: typing.Optional[str] = "HEAD",
    recursive: bool = True
) -> typing.Sequence[str]:
    """List DVC-tracked files

    Args:
        repo: the location of the DVC project, can be URL or file system path
        path: path to a file or directory to be listed
        rev: Git revision of the repository to list content for
        recursive: list contents of all subdirectories in path

    Returns:
        A list of the path (relative to the `path` argument) of the DVC-tracked files

    """
    cmd_args = ["dvc", "list", repo, path, "--rev", rev, "--dvc-only", "--json"]
    if recursive:
        cmd_args.append("-R")

    subprocess_res = subprocess.run(
        args=cmd_args,
        capture_output=True
    )

    if subprocess_res.returncode != 0:
        logger.error(
            f"Failed to list data files in {repo}/{path} with `dvc list`: "
            f"{subprocess_res.stderr}"
        )
        raise Exception(f"Failed to list DVC-tracked files in {repo}/{path}")

    data_files_info = json.loads(subprocess_res.stdout)
    data_files_path = [file_info['path'] for file_info in data_files_info]
    return data_files_path


def dvc_download_data_file(
    path: str,
    output_dir: typing.Union[Path, str],
    repo: typing.Optional[str] = None,
    rev: typing.Optional[str] = "HEAD",
    preserve_path: bool = False,
) -> None:
    """Download a data file using dvc

    path: location and file name of the target to be downloaded,
     relative to the root of the DVC project repo.
     Mirrors the parameter `path <https://dvc.org/doc/api-reference/read#parameters>`__
    output_dir: the folder to contains the downloaded file
    repo: the location of the DVC project, can be URL or file system path.
     Mirrors the parameter `repo <https://dvc.org/doc/api-reference/read#parameters>`__
    rev: Git revision of the data, defaults to HEAD.
     Mirrors the parameter `rev <https://dvc.org/doc/api-reference/read#parameters>`__
    preserve_path: preserve the path relative to the DVC repo when downloading,
     defaults to False
    """
    path = Path(path)
    if preserve_path:
        output_file_path = Path(output_dir).joinpath(path)
    else:
        output_file_path = Path(output_dir) / path.name
    output_file_path.parent.mkdir(parents=True, exist_ok=True)

    logger.info(f"Downloading file {path} at {rev} to {output_file_path}")
    with output_file_path.open(mode="wb") as f:
        f.write(dvc.api.read(path=path, repo=repo, rev=rev, mode="rb"))


def dvc_pull_data_in_folder(
    repo: typing.Union[Path, str],
    path: str,
    output_dir: typing.Union[Path, str],
    rev: typing.Optional[str] = "HEAD",
    recursive: bool = True,
    preserve_path: bool = False,
) -> None:
    """Download DVC-tracked files inside a directory

    Args:
        repo: the location of the DVC project, can be URL or file system path
        path: path to a directory to be downloaded
        output_dir: path to the folder to contains the downloaded file
        rev: Git revision of the repository to list content for, defaults to HEAD
        recursive: download contents of all subdirectories in path, defaults to True
        preserve_path: preserve the path of the data files when downloading,
         defaults to False
    """
    data_file_paths = dvc_list_tracked_files(
        repo=repo, path=path, rev=rev, recursive=recursive
    )
    for file_path in data_file_paths:
        dvc_download_data_file(
            path=f"{path}/{file_path}",
            repo=repo,
            rev=rev,
            output_dir=output_dir,
            preserve_path=preserve_path
        )


@click.command()
@click.option(
    "--repo",
    default=".",
    help="Location of the DVC repo",
)
@click.option(
    "--path",
    default="data",
    help="Path to folder containing the .dvc files, relative to repo",
)
@click.option(
    "--output-dir",
    "-o",
    default=".",
    help="Path to the folder containing the downloaded data",
)
def cli(
    repo: str,
    path: str,
    output_dir: str
):
    """This script preprocesses the data"""

    data_config = get_config(config_path="../conf", config_name="data")
    rev = data_config["git_revision"]

    logger.info(f"Download from at {rev} to {output_dir}")
    dvc_pull_data_in_folder(
        repo=repo, path=path, output_dir=output_dir, rev=rev, recursive=True
    )
    

if __name__ == "__main__":
    cli()
