from pathlib import Path

import click

from .logger import logger
from .types import PreprocessedData, RawData
from .utils import load_raw_data, save_processed_data


def preprocess_data(data: RawData) -> PreprocessedData:
    logger.info("Preprocessing data")
    raise NotImplementedError("Implement the code to preprocess the data")


def run_preprocess(input_data: Path, output_dir: Path) -> None:
    loaded_data = load_raw_data(input_data)

    preprocessed_data = preprocess_data(data=loaded_data)

    save_processed_data(data=preprocessed_data, output_dir=output_dir)


@click.command()
@click.option(
    "--input-data",
    type=click.Path(exists=True),
    default="./data/fashion-mnist",
    help="Path to the folder containing input data",
)
@click.option(
    "--output-dir",
    type=click.Path(),
    default=".",
    help="Path to the folder containing the preprocessed data",
)
def cli(
    input_data: str,
    output_dir: str,
):
    """This script preprocesses the data"""
    run_preprocess(
        input_data=Path(input_data).resolve(),
        output_dir=Path(output_dir).resolve()
    )
    return


if __name__ == "__main__":
    cli()
